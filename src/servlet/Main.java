package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.GetShouterListLogic;
import model.PostShouterLogic;
import model.Shouter;
import model.User;
/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException,
					IOException {

		//お叫びリストを取得して、リクエストスコープに保存
		GetShouterListLogic getShouterListLogic = new GetShouterListLogic();
		List<Shouter> shouterList = getShouterListLogic.execute();
		request.setAttribute("shouterList", shouterList);

		//ログインしているか確認するためセッションスコープからユーザー情報を奪取！
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		if(loginUser == null) {
		//ログインしていない場合リダイレクトよろしく！
			response.sendRedirect("/sakendaaa/");
		}else{

		//ログイン済みの場合フォワード
		}
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
			dispatcher.forward(request, response);

	}

	//doPostメソッド
		protected void doPost(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String text = request.getParameter("text");

		//お入力値チェック
		if(text != null && text.length() != 0){

			//セッションスコープに保存されたユーザー情報を取得
			HttpSession session = request.getSession();
			User loginUser = (User) session.getAttribute("loginUser");

			//お叫びをお叫びリストに追加

			Shouter shouter = new Shouter(loginUser.getName(),text);
			 PostShouterLogic postShouterLogic = new PostShouterLogic();
			 postShouterLogic.execute(shouter);

			//お叫びリストを取得してリクエストスコープに保存

		}else{
			//エラーメッセージをリクエストスコープに西武ライオンズ！
			request.setAttribute("errorMsg", "お叫びが入力されていません");
		}
		GetShouterListLogic getShouterListLogic = new GetShouterListLogic();
		List<Shouter> shouterList = getShouterListLogic.execute();
		request.setAttribute("shouterList", shouterList);
		//フォワード！ディスパッチャーあんたは偉い！がんばれ！
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
		dispatcher.forward(request, response);

	}
}

