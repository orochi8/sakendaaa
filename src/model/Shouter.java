package model;

import java.io.Serializable;

public class Shouter implements Serializable{
	private int id; //id
	private String userName; //user name
	private String text; //shout content

	public Shouter(){}
	public Shouter(String userName, String text){
		this.userName = userName;
		this.text = text;
	}


	public Shouter(int id, String userName, String text) {
		this.id = id;
		this.userName = userName;
		this.text = text;
	}

	public int getId() {return id;} //ここまで追加した!
	public String getUserName() {return userName;}
	public String getText() {return text;}

}
