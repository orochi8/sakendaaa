package model;

import java.util.List;

import dao.ShouterDAO;

public class GetShouterListLogic {
	public List<Shouter> execute(){
		ShouterDAO dao = new ShouterDAO();
		List<Shouter> shouterList = dao.findAll();
		return shouterList;
	}
}
