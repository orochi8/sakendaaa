package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Shouter;

public class ShouterDAO {
	//データベース接続に使用する情報
	final String DRIVER_NAME = "com.mysql.jdbc.Driver"; //MySQLドライバ
    final String DB_URL = "jdbc:mysql://localhost:3306/"; //DBサーバー名
    final String DB_NAME = "shouter"; //データベース名
    final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8"; //文字化け防止
    final String JDBC_URL =DB_URL+DB_NAME+DB_ENCODE; //接続DBとURL
    final String DB_USER = "root"; //ユーザーID
    final String DB_PASS = "root"; //パスワード

	public List<Shouter> findAll(){
		List<Shouter> shouterList = new ArrayList<>();

		//database connect
		try {
			Class.forName(DRIVER_NAME);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}

		try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)) {
			//select文の準備
			String sql = "SELECT ID,NAME,TEXT FROM SAKENDAAA ORDER BY ID DESC";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			//selectを実行
			ResultSet rs = pStmt.executeQuery();

			//select文の結果をArrayListに格納
			while (rs.next()){
				int id = rs.getInt("ID");
				String userName = rs.getString("NAME");
				String text = rs.getString("TEXT");
				Shouter shouter = new Shouter(id,userName,text);
				shouterList.add(shouter);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return shouterList;
	}
	public boolean create(Shouter shouter){
		//Database Connect
		try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)){

		//insert文の準備コマネチ！
			String sql = "INSERT INTO SAKENDAAA(NAME,TEXT) VALUES(?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);

		//insert文中の「？」に使用する値を設定しSQLを完成
			pStmt.setString(1, shouter.getUserName());
			pStmt.setString(2, shouter.getText());

		//insert文を実行(resultには追加された行数が代入される)
			int result = pStmt.executeUpdate();
			if (result != 1){
				return false;
			}

		}catch (SQLException e) {
			e.printStackTrace();
			return false;

		}
		return true;
	}
}
