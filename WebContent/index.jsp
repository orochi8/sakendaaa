<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- //一応、授業で習った内容を各部に散らばせて作りました //-->

<!DOCTYPE html>
<html>
<head>
<meta name="description" content="ストレス社会にどっぷり嵌まり込んだ人々に心に溜まった鬱憤を思う存分吐き出させる叫びの専門サイト" />
<meta name="keywords" content="ストレス,叫び,鬱憤,精神,発散,叫ぶ,おんどりゃぁぁ,サケンダー" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="./css/style.css" media="screen">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">

<title>思いっきり叫びたい時にその思いを投稿するサイト｜叫んでストレス発散！サケンダー！</title>
</head>
<body>
<div align="center">
<!--// h1にはグラデーションのCSSを使って、より雰囲気を出したったー！ //-->
<h1 class="fonfa">迷える子羊よ叫びたまえぇぇ</h1>
<h2 class="fonfas">It will be best site where if you wanna really shout!</h2>
<jsp:include page="./header.jsp" />
<!--// フォームにはCSSやformパーツ使って装飾したぜ！ //-->
<form action="/sakendaaa/Login" method="post" class="formf">
ストレス解消用ユーザー名：<input type="text" name="name" class="inpback" placeholder="好きなID"><br />
ストレス発破用パスワード：<input type="text" name="pass" class="inpback" placeholder="0000"><br />
<input type="image" src="./img/loginb.jpg" alt="クリックしてみぃ！">
</form>
<!--// グラデーションのhr //-->
<hr class="style-grd" />
<jsp:include page="./footer.jsp" />
</div>
</body>
</html>