<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="./css/style.css" media="screen">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
  var pagetop = $('.pagetop');
    $(window).scroll(function () {
       if ($(this).scrollTop() > 400) {
            pagetop.fadeIn();
       } else {
            pagetop.fadeOut();
            }
       });
       pagetop.click(function () {
           $('body, html').animate({ scrollTop: 0 }, 600);
              return false;
   });
});
</script>

<title>思う存分叫びなはれ！</title>
</head>
<body>
	<div align="center" id="top">
		<h1 class="fonfa">迷える子羊よ叫びたまえぇぇ</h1>
		<h2 class="fonfas">Shouting shouting shouting! You go more and more!!</h2>
		<jsp:include page="../../header.jsp" />
		<strong><c:out value="${loginUser.name}" />さんがログイン中ですわ！</strong>
		<h3>大切な事なので３回叫んでみましょう</h3>
		<!-- とってもホラー風のtextareaとボタンをCSSでデザイン -->
		<form action="/sakendaaa/Main" method="post">
		<input type="text" name="text" class="sakebitxt">
		<input id="submit_button" type="submit" name="submit" value="お叫びの投稿">
		</form>
		<hr class="style-grd" />
		<c:if test="${not empty errorMsg}">
		<p>${errorMsg}</p>
		</c:if>
		<div class="sakebiarea">
		<c:forEach var="shouter" items="${shouterList}">
			<span class="username">【叫び人：<c:out value="${shouter.userName}" />さん】</span><br />
			  <span class="usertxt"><c:out value="${shouter.text}" />…</span><br />
		    <span class="usertxt1"><c:out value="${shouter.text}" />！</span><br />
		    <span class="kaom">( ｣｀Д´)」＜</span><span class="usertxt2"><c:out value="${shouter.text}" />！！</span>
			<hr class="dotline" />
		</c:forEach>
		</div>
		<!--//ログアウトやリロードはボタンじゃなくfont-awesomeのアイコンを使いました！//-->
		<p>
			<a class="icon" href="/sakendaaa/Main"><i class="fas fa-redo fa-2x"></i></a>
			<a class="icon" href="/sakendaaa/Logout"><i class="fas fa-sign-out-alt fa-2x"></i></a>
		</p>
		<hr class="style-grd" />
		<!-- //jQueryで上にスクロールできるTotopボタンを設置// -->
		<p class="pagetop"><a href="#top">▲</a></p>
		<jsp:include page="../../footer.jsp" />
	</div>
</body>
</html>