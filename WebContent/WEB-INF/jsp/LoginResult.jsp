<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="model.User" %>
    <%
    //セッションスコープからユーザー情報を取得
    User loginUser = (User) session.getAttribute("loginUser");
    %>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="./css/style.css" media="screen">

<title>叫んだー！ログインできたんかー！</title>
</head>
<body>
<div align="center">
<!--// h1はCSSでグラデーションにして海外の怪しいサイトみたいな雰囲気にする //-->
<h1 class="fonfa">迷える子羊よ叫びたまえぇぇ</h1>
<h2 class="fonfas">If you wanna really scream which will be login here!</h2>
<jsp:include page="../../header.jsp" />
<% if (loginUser != null) {%>
<strong>ログインに成功しましたったー！</strong>
<p>ようこそ！叫びたい <%= loginUser.getName() %>さん！<br />▼</p>
<a href="/sakendaaa/Main"><img src="./img/sakebib.jpg" alt="お叫びたいあなたの入り口"></a>
<% } else { %>
<p>ログインに失敗しました！</p>
<!--// 失敗ボタンはCSSで作ったものですわ //-->
<a href="/sakendaaa/" class="linkb">トップヘ戻る</a>
<% } %>
<hr class="style-grd" />
<jsp:include page="../../footer.jsp" />
</div>
</body>
</html>